# halloc

### 简述
基于多线程的高效内存管理模块，可以快速的申请释放内存块，同时对于内存块有监控作用，还包括一个垃圾回收插件

### 功能
>* 支持快速的申请内存，下面会有与malloc的性能比较
>* 支持内存越界检查
>* 支持内存泄漏检测
>* 支持泄漏内存的垃圾回收插件

### 展示
#### 同时申请后释放的halloc和malloc花费时间（ms）比较 
halloc最快速的情况下比malloc快一个数量级
![image](https://gitee.com/dcp_483/halloc/raw/master/jpg/1.JPG)
![image](https://gitee.com/dcp_483/halloc/raw/master/jpg/2.JPG)

#### 同时申请后不释放的halloc和malloc花费时间（ms）比较
halloc最慢的情况下比malloc保持在相同数量级
![image](https://gitee.com/dcp_483/halloc/raw/master/jpg/4.JPG)
![image](https://gitee.com/dcp_483/halloc/raw/master/jpg/3.JPG)


### 问题


### 联系
* **当出现问题以及bug时欢迎联系 邮箱:garsonzhang@foxmail.com**
- 